import 'react-native-gesture-handler';
import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {Provider} from 'react-redux';
import thunk from 'redux-thunk';
import {createStore, applyMiddleware} from 'redux';
import mainReducer from './src/store/reducer/main';
import MainScreen from './src/screens/MainScreen';
import PostsScreen from './src/screens/PostsScreen';
import DetailsScreen from './src/screens/DetailsScreen';
import SettingsScreen from './src/screens/SettingsScreen';

const Stack = createStackNavigator();
const store = createStore(mainReducer, applyMiddleware(thunk));

const App = () => {
  return (
    <Provider store={store}>
      <NavigationContainer>
        <Stack.Navigator
          initialRouteName="MainScreen"
          screenOptions={{
            headerTitleStyle: {
              textAlign: 'center',
            },
          }}>
          <Stack.Screen
            name="MainScreen"
            component={MainScreen}
            options={{title: 'Welcome'}}
          />
          <Stack.Screen
            name="Tweets"
            component={PostsScreen}
            options={() => ({
              gestureEnabled: false,
              headerLeft: () => null,
            })}
          />
          <Stack.Screen name="Tweet" component={DetailsScreen} />
          <Stack.Screen name="Settings" component={SettingsScreen} />
        </Stack.Navigator>
      </NavigationContainer>
    </Provider>
  );
};

export default App;
