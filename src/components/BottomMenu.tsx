import React from 'react';
import {StyleSheet, View, Text, TouchableOpacity} from 'react-native';

interface IProps {
  nickName?: string;
  actions: any;
  navigation: any;
}

const BottomMenu = (props: IProps) => {
  return (
    <View>
      <View style={styles.container}>
        <TouchableOpacity onPress={() => props.navigation.navigate('Tweets')}>
          <Text>Home</Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={() => props.navigation.navigate('Settings')}>
          <Text>Settings</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};
export default BottomMenu;

const styles = StyleSheet.create({
  container: {
    width: '100%',
    height: '100%',
    alignItems: 'center',
    backgroundColor: 'white',
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: '10%',
  },
});
