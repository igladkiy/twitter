import React from 'react';
import {StyleSheet, View, Text, SafeAreaView} from 'react-native';
import {bindActionCreators} from 'redux';
import * as actions from '../store/actions/main';
import {connect} from 'react-redux';
import {StateProps} from '../store/types/main';
import BottomMenu from '../components/BottomMenu';

interface IProps {
  nickName?: string;
  actions: any;
  navigation: any;
  route: any;
}

const PostsScreen = (props: IProps) => {
  const {item} = props.route.params;
  return (
    <SafeAreaView style={styles.safeStyle}>
      <View style={styles.container}>
        <Text>{item.text}</Text>
      </View>
      <View style={styles.menu}>
        <BottomMenu {...props} />
      </View>
    </SafeAreaView>
  );
};

export default connect(
  (state: StateProps) => ({
    nickName: state.nickName,
  }),
  dispatch => ({
    actions: bindActionCreators(actions, dispatch),
  }),
)(PostsScreen);

const styles = StyleSheet.create({
  container: {
    width: '100%',
    height: '93%',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#f7f7f7',
    padding: 15,
  },
  menu: {
    width: '100%',
    height: '7%',
  },
  safeStyle: {
    backgroundColor: 'white',
  },
});
