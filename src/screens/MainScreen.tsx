import React from 'react';
import {StyleSheet, Alert, TextInput, View, Button} from 'react-native';
import {bindActionCreators} from 'redux';
import * as action from '../store/actions/main';
import {connect} from 'react-redux';
import {StateProps} from '../store/types/main';

interface IProps {
  nickName?: string;
  actions: any;
  navigation: any;
}

const MainScreen = ({nickName = '', actions, navigation}: IProps) => {
  const setNickname = (name: String) => {
    actions.setNickname(name);
  };

  const goToPosts = () => {
    if (nickName && nickName.length > 3) {
      if (nickName.indexOf('admin') > -1 || nickName.indexOf('twitter') > -1) {
        Alert.alert('Choose another nickname, please');
      } else {
        navigation.navigate('Tweets');
      }
    } else {
      Alert.alert('Please enter valid data');
    }
  };
  return (
    <View>
      <View style={styles.container}>
        <View style={styles.form}>
          <TextInput
            style={styles.input}
            onChangeText={text => setNickname(text.replace(/[^a-z1-9_]/g, ''))}
            value={nickName}
            placeholder="User name"
            maxLength={15}
          />
          <Button
            disabled={nickName.length < 4}
            onPress={goToPosts}
            title={'Login'}
          />
        </View>
      </View>
    </View>
  );
};

export default connect(
  (state: StateProps) => ({
    nickName: state.nickName,
  }),
  dispatch => ({
    actions: bindActionCreators(action, dispatch),
  }),
)(MainScreen);

const styles = StyleSheet.create({
  container: {
    width: '100%',
    height: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#f7f7f7',
  },
  form: {
    height: 250,
    width: 250,
    justifyContent: 'center',
    alignItems: 'center',
  },
  input: {
    height: 50,
    marginBottom: 20,
    width: 250,
    borderColor: 'gray',
    borderWidth: 2,
    borderRadius: 15,
    backgroundColor: 'white',
    paddingHorizontal: 10,
  },
});
