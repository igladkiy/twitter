import React, { useEffect, useState } from "react";
import {
  StyleSheet,
  View,
  Text,
  FlatList,
  SafeAreaView,
  TouchableOpacity,
  ActivityIndicator,
} from "react-native";
import { bindActionCreators } from "redux";
import * as actions from "../store/actions/main";
import { connect } from "react-redux";
import { StateProps } from "../store/types/main";
import BottomMenu from "../components/BottomMenu";

interface IProps {
  nickName?: string;
  data?: Array<Object>;
  actions: any;
  navigation: any;
  loading: boolean;
  paginationKey: string;
}

const DetailsScreen = (props: IProps) => {
  useEffect(() => props.actions.getData(), []);
  useEffect(() => setLoading(props.loading), [props.loading]);

  const [loading, setLoading] = useState(false);

  const renderItem = ({ item }) => (
    <TouchableOpacity
      style={styles.tweetCont}
      onPress={() => props.navigation.navigate("Tweet", { item: item })}
    >
      <Text>{item.text}</Text>
    </TouchableOpacity>
  );

  return (
    <SafeAreaView style={styles.safeStyle}>
      {loading && (
        <View style={styles.loader}>
          <ActivityIndicator size="large" color={"black"} />
        </View>
      )}
      <View style={styles.container}>
        <FlatList
          initialNumToRender={20}
          data={props.data}
          onEndReachedThreshold={0.1}
          keyExtractor={(item) => item.id}
          onEndReached={({ distanceFromEnd }) => {
            console.log(distanceFromEnd);
            distanceFromEnd < 90 && props.actions.getData(props.paginationKey);
          }}
          renderItem={renderItem}
        />
      </View>
      <View style={styles.menu}>
        <BottomMenu {...props} />
      </View>
    </SafeAreaView>
  );
};

export default connect(
  (state: StateProps) => ({
    nickName: state.nickName,
    data: state.data,
    loading: state.loading,
    paginationKey: state.paginationKey,
  }),
  (dispatch) => ({
    actions: bindActionCreators(actions, dispatch),
  })
)(DetailsScreen);

const styles = StyleSheet.create({
  container: {
    width: "100%",
    height: "93%",
    justifyContent: "center",
    backgroundColor: "#f7f7f7",
    padding: 15,
  },
  menu: {
    width: "100%",
    height: "7%",
  },
  itemContainer: {
    width: "90%",
    marginHorizontal: "5%",
    backgroundColor: "white",
    height: 100,
    flexDirection: "row",
    marginVertical: "2%",
    borderRadius: 10,
  },
  loginBtn: {
    width: 150,
    height: 50,
    borderRadius: 15,
    backgroundColor: "silver",
    justifyContent: "center",
    alignItems: "center",
  },
  form: {
    height: 250,
    width: 250,
    justifyContent: "center",
    alignItems: "center",
  },
  input: {
    height: 50,
    marginBottom: 20,
    width: 250,
    borderColor: "gray",
    borderWidth: 2,
    borderRadius: 15,
    backgroundColor: "white",
    paddingHorizontal: 10,
  },
  loginTxt: {
    color: "white",
  },
  tweetCont: {
    width: "100%",
    minHeight: 50,
    paddingHorizontal: 10,
    backgroundColor: "#D3D3D3",
    borderRadius: 10,
    marginVertical: 10,
    paddingVertical: 10,
  },
  safeStyle: {
    backgroundColor: "white",
  },
  loader: {
    position: "absolute",
    top: 0,
    width: "100%",
    height: "93%",
    zIndex: 100,
    justifyContent: "center",
  },
});
