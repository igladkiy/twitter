import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  SafeAreaView,
} from 'react-native';
import {bindActionCreators} from 'redux';
import * as actions from '../store/actions/main';
import {connect} from 'react-redux';
import {StateProps} from '../store/types/main';
import BottomMenu from '../components/BottomMenu';

interface IProps {
  nickName?: string;
  id?: string;
  actions: any;
  navigation: any;
}

const SettingsScreen = (props: IProps) => {
  const logOut = () => {
    props.actions.logout();
    props.navigation.navigate('MainScreen');
  };
  return (
    <SafeAreaView style={styles.safeStyle}>
      <View style={styles.container}>
        <Text style={styles.txt}>
          <Text style={styles.boldTxt}>ID:</Text> {props.id}
        </Text>
        <Text style={styles.txt}>
          <Text style={styles.boldTxt}>Nickname:</Text> {props.nickName}
        </Text>
        <TouchableOpacity style={styles.logoutBtn} onPress={logOut}>
          <Text style={styles.logoutTxt}>Log out</Text>
        </TouchableOpacity>
      </View>
      <View style={styles.menu}>
        <BottomMenu {...props} />
      </View>
    </SafeAreaView>
  );
};

export default connect(
  (state: StateProps) => ({
    nickName: state.nickName,
    id: state.id,
  }),
  dispatch => ({
    actions: bindActionCreators(actions, dispatch),
  }),
)(SettingsScreen);

const styles = StyleSheet.create({
  container: {
    width: '100%',
    height: '93%',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#f7f7f7',
  },
  logoutBtn: {
    width: 150,
    height: 50,
    borderRadius: 15,
    backgroundColor: 'gray',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 40,
  },
  logoutTxt: {
    color: 'white',
  },
  txt: {
    fontSize: 20,
  },
  boldTxt: {
    fontWeight: 'bold',
  },
  menu: {
    width: '100%',
    height: '7%',
  },
  safeStyle: {
    backgroundColor: 'white',
  },
});
