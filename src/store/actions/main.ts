import * as types from "../types/main";
import { getTweets } from "../api/main";

export const setNickname = (name: string) => ({
  type: types.SET_NICKNAME,
  name,
});

export const logout = () => ({
  type: types.LOGOUT,
});

export const setIsLoading = (loading: boolean) => ({
  type: types.SET_LOADING,
  loading,
});

const setData = (data: Array<Object>) => ({
  type: types.SET_DATA,
  data,
});

const setNextPaginationKey = (data: string) => ({
  type: types.SET_NEXT_PAGINATION_KEY,
  data,
});

export const getData = (next_token: string = "") => (dispatch) => {
  dispatch(setIsLoading(true));
  const response = getTweets(next_token);
  response
    .then((res) => {
      dispatch(setData(res.data.data));
      dispatch(setNextPaginationKey(res.data.meta.next_token));
      dispatch(setIsLoading(false));
    })
    .catch((err) => {
      console.log(err);
      dispatch(setIsLoading(false));
    });
};
