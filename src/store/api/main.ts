import axios from "axios";
import { token } from "../../config/config";

export const getTweets = (key: string) => {
  const pagination = !!key ? `&pagination_token=${key}` : "";
  return axios.get(
    `https://api.twitter.com/2/users/732521058507620356/tweets?max_results=20${pagination}`,
    {
      headers: {
        Authorization: `Bearer ${token}`,
        "Content-Type": "application/json",
        Accept: "application/json",
      },
    }
  );
};
