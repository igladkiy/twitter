import * as types from "../types/main";
import { AnyAction } from "redux";

const initState = {
  nickName: "",
  id: "732521058507620356",
  loading: false,
  data: [],
  paginationKey: "",
};

export default (state = initState, action: AnyAction) => {
  switch (action.type) {
    case types.SET_NICKNAME:
      return {
        ...state,
        nickName: action.name,
      };
    case types.SET_DATA:
      return {
        ...state,
        data: [...state.data, ...action.data],
      };
    case types.SET_LOADING:
      return {
        ...state,
        loading: action.loading,
      };
    case types.LOGOUT:
      return {
        ...state,
        nickName: "",
        paginationKey: "",
        data: [],
      };
    case types.SET_NEXT_PAGINATION_KEY:
      return {
        ...state,
        paginationKey: action.data,
      };
    default:
      return state;
  }
};
