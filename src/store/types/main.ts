export const SET_NICKNAME = "SET_NICKNAME";
export const LOGOUT = "LOGOUT";
export const SET_DATA = "SET_DATA";
export const SET_LOADING = "SET_LOADING";
export const SET_NEXT_PAGINATION_KEY = "SET_NEXT_PAGINATION_KEY";

export interface StateProps {
  nickName: string;
  id: string;
  loading: boolean;
  data: Array<String>;
  paginationKey: string;
}
